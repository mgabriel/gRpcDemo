package cern.grpc.demo;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;
import static io.grpc.Metadata.BINARY_BYTE_MARSHALLER;

import io.grpc.Metadata;

/**
 * Constants for the gRPC header data
 */
public class HeaderConstants {
    public static final Metadata.Key<String> TRACE_ID_KEY = Metadata.Key.of("traceId", ASCII_STRING_MARSHALLER);
    public static final Metadata.Key<String> APPLICATION_ID_KEY = Metadata.Key.of("applicationId", ASCII_STRING_MARSHALLER);
    public static final Metadata.Key<byte[]> RBAC_TOKEN_KEY = Metadata.Key.of("rbacToken-bin", BINARY_BYTE_MARSHALLER);
}

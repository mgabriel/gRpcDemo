package cern.grpc.demo;

import static java.util.concurrent.TimeUnit.SECONDS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.util.traceid.TraceIds;
import cern.accsoft.security.rba.login.LoginPolicy;
import cern.accsoft.stream.commons.util.grpc.GrpcRetryFlux;
import cern.rba.util.login.RbaAuthenticator;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class DemoClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoClient.class);
    private static String host = "localhost";
    private static int port = 9000;

    private static ReactorDemoServiceGrpc.ReactorDemoServiceStub createDemoStub() {
        ManagedChannelBuilder<?> channelBuilder = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .intercept(new AccsoftClientInterceptor());
        final ManagedChannel channel;
        channel = channelBuilder.build();
        return ReactorDemoServiceGrpc.newReactorStub(channel);
    }

    private static Flux<DemoResult> getDemoStream(DemoStreamRequest request) {
        return new GrpcRetryFlux<>(() -> createDemoStub()
                .getDemoStream(Mono.just(request).doOnSubscribe(c -> createContext()))
                .doOnError(err -> LOGGER.info("Server disconnected ", err))
        );
    }

    private static Mono<DemoResult> getSingleResult(DemoRequest request) {
        return createDemoStub().getSingleResult(Mono.just(request));
    }

    private static Mono<DemoResult> getSingleResultSlowService(DemoRequest request) {
        return createDemoStub().getSingleResultSlowService(Mono.just(request));
    }

    private static void createContext() {
        TraceIds.setNewTraceId();
    }

    public static void main(String[] args) throws Exception {
        RbaAuthenticator.login(LoginPolicy.LOCATION);
        DemoRequest requestSingle = DemoRequest.newBuilder().setRequest("single request Hello").build();
        DemoStreamRequest request = DemoStreamRequest.newBuilder().setRequest("Hello").build();
        getSingleResult(requestSingle).map(DemoResult::getResult).doOnNext(LOGGER::info).block();
        SECONDS.sleep(1);
        getSingleResultSlowService(requestSingle).map(DemoResult::getResult).doOnNext(LOGGER::info).block();

        getDemoStream(request).map(DemoResult::getResult).doOnNext(LOGGER::info).blockLast();
    }
}

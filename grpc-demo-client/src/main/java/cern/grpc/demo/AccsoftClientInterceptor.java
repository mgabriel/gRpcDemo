package cern.grpc.demo;

import static cern.grpc.demo.HeaderConstants.APPLICATION_ID_KEY;
import static cern.grpc.demo.HeaderConstants.RBAC_TOKEN_KEY;
import static cern.grpc.demo.HeaderConstants.TRACE_ID_KEY;

import java.io.Serializable;

import cern.accsoft.commons.util.proc.ProcUtils;
import cern.accsoft.commons.util.proc.ProcessInfo;
import cern.accsoft.commons.util.rba.RbaReflectiveInvoker;
import cern.accsoft.commons.util.traceid.TraceIds;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import org.apache.commons.lang3.SerializationUtils;

/**
 * @author mgabriel.
 */
public class AccsoftClientInterceptor implements ClientInterceptor{

    private static final RbaReflectiveInvoker rbaReflectiveInvoker = new RbaReflectiveInvoker();

    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method,
            CallOptions callOptions, Channel next) {
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {
            @Override
            public void start(Listener<RespT> responseListener, Metadata headers) {
                String traceId = TraceIds.getTraceId();
                if(traceId != null){
                    headers.put(TRACE_ID_KEY, traceId);
                }
                ProcessInfo processInfo = ProcUtils.get().getProcessInfo();
                String officialApplicationId = processInfo.getOfficialApplicationId();
                if(officialApplicationId != null){
                    headers.put(APPLICATION_ID_KEY, officialApplicationId);
                }
                Serializable token = rbaReflectiveInvoker.findRbaToken();
                if(token != null){
                    headers.put(RBAC_TOKEN_KEY, SerializationUtils.serialize(token));
                }
                super.start(responseListener, headers);
            }
        };
    }
}

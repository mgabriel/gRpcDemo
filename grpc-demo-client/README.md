# Java client for the demo gRPC service

## Running the client

Run the [DemoClient](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/client/src/main/java/cern/grpc/demo/DemoClient.java) class.

This class uses an [interceptor](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/client/src/main/java/cern/grpc/demo/AccsoftClientInterceptor.java) to add the RBAC token to the requests sent to the server.
If the RBAC login by location is not enabled on your machine, you will have to change the LoginPolicy to `EXPLICIT`

## Configuration
In order to see output you have to add the path to log4j2 configuration 
e.g. with the VM Option: `-Dlog4j.configurationFile=log4j2.xml` 

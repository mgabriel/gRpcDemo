# A demo gRPC server

## Running the server

This demo provides different runnable examples:
 
- [raw gRPC](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/raw/ManualRawServerStarter.java) in the file `cern.grpc.demo.raw.ManualRawServerStarter` : this is an example of a gRPC server started manually, with the service implemented in pure gRPC style
- [gRPC with reactive streams](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/reactor/ManualFluxServerStarter.java) : this is an example of a gRPC server started manually, with the service (`cern.grpc.demo.reactor.DemoService`) implemented with the [reactor-core](http://projectreactor.io/docs/core/release/reference/) implementation of [reactive streams](https://en.wikipedia.org/wiki/Reactive_Streams)
- A [Spring Boot](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/SpringBootServerStarter.java) starter: this is the cleaner way to start gRPC as a service in Spring Boot. This example also adds some instrumentation thanks to gRPC interceptors:
    - The [AccsoftServerInterceptor](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/AccsoftServerInterceptor.java) retrieves the RBAC token and Application ID from the gRPC metadata (similar to what we are doing with RMI)
    - The [ZipkinInterceptor](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/ZipkinInterceptor.java) is a third party interceptor, it adds tracing to the gRPC invocations and sends the traces to an external Zipkin server (this part is optional)

## Configuration

- Spring Boot starter
    - The server starts by default on port 9000 you can tweak the `grpc.port` and other settings in the [properties file](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/resources/application-default.properties).
- Manual staters
    - You can change the port through a static field `PORT`.
    - in order to see any output you have to add the path to log4j2 configuration e.g. with the VM Option: `-Dlog4j.configurationFile=log4j2-dev.xml` 

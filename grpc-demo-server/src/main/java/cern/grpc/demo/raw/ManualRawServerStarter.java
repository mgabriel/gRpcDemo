package cern.grpc.demo.raw;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.grpc.demo.DemoRequest;
import cern.grpc.demo.DemoResult;
import cern.grpc.demo.DemoServiceGrpc;
import cern.grpc.demo.DemoStreamRequest;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;
import io.grpc.stub.StreamObserver;

/**
 * @author mgabriel.
 */
@SuppressWarnings("Duplicates")
public class ManualRawServerStarter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManualRawServerStarter.class);
    private static final int PORT = 9000;

    private static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    public static void main(String[] args) throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        LOGGER.info("Starting gRPC server on port {}", PORT);
        Server server = ServerBuilder.forPort(PORT)
                .addService(ServerInterceptors.intercept(new DemoService()))
                .build()
                .start();
        LOGGER.info("GRPC Server started, listening on {}", PORT);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOGGER.info("*** shutting down gRPC server since JVM is shutting down");
            if (server != null) {
                server.shutdown();
                latch.countDown();
            }
            LOGGER.info("*** server shut down");
        }));
        latch.await();
    }

    private static class DemoService extends DemoServiceGrpc.DemoServiceImplBase{

        @Override
        public void getDemoStream(DemoStreamRequest request, StreamObserver<DemoResult> responseObserver) {
            String requestString = request.getRequest();
            AtomicInteger i = new AtomicInteger(0);
            executorService.scheduleAtFixedRate(() -> responseObserver.onNext(DemoResult.newBuilder().setResult(requestString+ " world "+i.incrementAndGet()).build()), 0, 1, SECONDS);
        }

        @Override
        public void getSingleResult(DemoRequest request, StreamObserver<DemoResult> responseObserver) {
            DemoResult result = DemoResult.newBuilder().setResult(request.getRequest() + " world ").build();
            responseObserver.onNext(result);
        }

        @Override
        public void getSingleResultSlowService(DemoRequest request, StreamObserver<DemoResult> responseObserver) {
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            getSingleResult(request, responseObserver);
        }
    }
}

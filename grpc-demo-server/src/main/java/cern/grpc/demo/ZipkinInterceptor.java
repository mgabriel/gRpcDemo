package cern.grpc.demo;

import brave.Tracing;
import brave.grpc.GrpcTracing;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import org.lognet.springboot.grpc.GRpcGlobalInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import zipkin2.Span;
import zipkin2.reporter.Reporter;

@GRpcGlobalInterceptor
public class ZipkinInterceptor implements ServerInterceptor {

    @Autowired
    private Tracing tracing;


    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers,
            ServerCallHandler<ReqT, RespT> next) {
        return GrpcTracing.create(tracing).newServerInterceptor().interceptCall(call, headers, next);
    }
}

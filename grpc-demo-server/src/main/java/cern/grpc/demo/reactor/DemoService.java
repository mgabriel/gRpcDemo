package cern.grpc.demo.reactor;

import static java.time.Duration.ofSeconds;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import brave.Span;
import brave.Tracer;
import brave.Tracing;
import cern.grpc.demo.DemoRequest;
import cern.grpc.demo.DemoResult;
import cern.grpc.demo.DemoStreamRequest;
import cern.grpc.demo.ReactorDemoServiceGrpc;
import cern.grpc.demo.SlowService;
import cern.rba.util.lookup.RbaTokenLookup;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@GRpcService // This annotation registers the grpc service in the spring boot application
public class DemoService extends ReactorDemoServiceGrpc.DemoServiceImplBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoService.class);



    @Autowired
    private SlowService slowService;

    @Override
    public Flux<DemoResult> getDemoStream(Mono<DemoStreamRequest> request) {
        logRbacToken();
        return request.flatMapMany(r -> Flux.interval(ofSeconds(1))
                .map(i -> DemoResult.newBuilder().setResult(r.getRequest() + " world  " + i).build()));
    }

    private void logRbacToken() {
        LOGGER.info("found rba token {}", RbaTokenLookup.findRbaToken());
    }

    @Override
    public Mono<DemoResult> getSingleResult(Mono<DemoRequest> request) {
        logRbacToken();
        return request.map(r -> DemoResult.newBuilder().setResult(r.getRequest() + " world").build());
    }

    @Override
    public Mono<DemoResult> getSingleResultSlowService(Mono<DemoRequest> request) {
        slowService.doSomething();
        return getSingleResult(request);
    }
}
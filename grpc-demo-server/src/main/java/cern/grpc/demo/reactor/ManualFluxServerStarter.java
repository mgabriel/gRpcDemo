package cern.grpc.demo.reactor;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;

/**
 * @author mgabriel.
 */
@SuppressWarnings("Duplicates")
public class ManualFluxServerStarter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManualFluxServerStarter.class);
    private static final int PORT = 9000;

    public static void main(String[] args) throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        LOGGER.info("Starting gRPC server on port {}", PORT);
        Server server = ServerBuilder.forPort(PORT)
                .addService(ServerInterceptors.intercept(new DemoService()))
                .build()
                .start();
        LOGGER.info("GRPC Server started, listening on {}", PORT);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOGGER.info("*** shutting down gRPC server since JVM is shutting down");
            if (server != null) {
                server.shutdown();
                latch.countDown();
            }
            LOGGER.info("*** server shut down");
        }));
        latch.await();
    }
}

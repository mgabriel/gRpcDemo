package cern.grpc.demo;

import static cern.grpc.demo.HeaderConstants.RBAC_TOKEN_KEY;
import static cern.grpc.demo.HeaderConstants.TRACE_ID_KEY;
import static org.apache.commons.lang3.SerializationUtils.deserialize;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.util.rba.RbaReflectiveInvoker;
import cern.accsoft.commons.util.traceid.TraceIds;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import org.lognet.springboot.grpc.GRpcGlobalInterceptor;

/**
 * This global interceptor will be applied to all gRPC services configured with Spring Boot
 */
@GRpcGlobalInterceptor
public class AccsoftServerInterceptor implements ServerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccsoftServerInterceptor.class);
    private static final RbaReflectiveInvoker rbaReflectiveInvoker = new RbaReflectiveInvoker();

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers,
            ServerCallHandler<ReqT, RespT> next) {
        String applicationId = headers.get(HeaderConstants.APPLICATION_ID_KEY);
        if (applicationId != null) {
            LOGGER.info("applicationId id found {}", applicationId);
        }

        //String traceId = headers.get(TRACE_ID_KEY);
        Serializable token;

        if (headers.containsKey(RBAC_TOKEN_KEY) && headers.get(RBAC_TOKEN_KEY) != null) {
            token = deserialize(headers.get(RBAC_TOKEN_KEY));
        }else{
            token = null;
        }

        ServerCall.Listener<ReqT> listener = next.startCall(call, headers);

        return new ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT>(listener) {
            @Override
            public void onMessage(ReqT message) {
                try {
                    setContext();
                    super.onMessage(message);
                }finally {
                   cleanup();
                }
            }

            private void setContext() {
//                if (traceId != null) {
//                    TraceIds.setTraceId(traceId);
//                }
                if(token != null){
                    rbaReflectiveInvoker.setMiddleTierRbaToken(token);
                }else{
                    rbaReflectiveInvoker.clearMiddleTierRbaToken();
                }
            }



            @Override
            public void onHalfClose() {
                try {
                    setContext();
                    super.onHalfClose();
                } finally {
                    cleanup();
                }
            }

            @Override
            public void onCancel() {
                try {
                    setContext();
                    super.onCancel();
                } finally {
                    cleanup();
                }
            }

            @Override
            public void onComplete() {
                try {
                    setContext();
                    super.onComplete();
                } finally {
                    cleanup();
                }
            }

            @Override
            public void onReady() {
                try {
                    setContext();
                    super.onReady();
                } finally {
                    cleanup();
                }
            }

            private void cleanup() {
//                TraceIds.clearTraceId();
                rbaReflectiveInvoker.clearMiddleTierRbaToken();
            }
        };
    }
}

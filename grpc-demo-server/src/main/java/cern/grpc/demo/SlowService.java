package cern.grpc.demo;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.TimeUnit;

import brave.Span;
import brave.Tracer;
import brave.Tracing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

/**
 * @author mgabriel.
 */
@Service
public class SlowService {

    @Autowired
    private Tracing tracing;

    @NewSpan(name = "slow service span") // this creates a new span in Zipkin
    public void doSomething() {
        createNewSpanForFun();
        try {
            SECONDS.sleep(3);
        } catch (InterruptedException e) {
            //
        }
    }

    // demonstrates the creation of nested span in Zipkin
    private void createNewSpanForFun() {
        if (tracing != null) {
            Tracer tracer = tracing.tracer();
            // this is how we can create a span manually
            Span newSpan = tracer.nextSpan().name("nested span in the slow service");
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(newSpan.start())) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } finally {
                newSpan.finish();
            }

        }
    }
}

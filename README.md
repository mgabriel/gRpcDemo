# A demo project exposing a gRPC service

## Configure the project

- Check out this repository and run `bob idea` or `bob eclipse`
- Since the projects have not been released with CBNG, ou need to manually configure a dependency on the 'domain' module in your ide in the 'client' and 'server' projects
- Go to the 'domain' project and run `bob generateProto`  (this will generate the java stubs and encoders/decoders, this part is not integrated in CBNG yet so it relies on config in a [build.gradle](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/domain/build.gradle) file)
- If it is not done automatically, add the subfolders of domain/build/generated/source/proto/main as source folders (or generated source folders) in your ide (there should be 3 folders: grpc, java and reactor)
- At this point the project should compile

- :snake: For the python configuration, check the [python guide](/python-client/)
 
## Running the example

- Running the [server](/server/)
- Running the [java client](/client/)
- Running the [python client](/python-client/)

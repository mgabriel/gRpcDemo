# Python client for the demo gRPC service

## Installation instructions

- You will need pip to install the gRPC module, either on your machine or in a python virtual env
    - To install it run: `sudo yum --disablerepo="*" --enablerepo="epel" install python-pip`  (sudo is not necessary in a virtual env)

- To install the gRPC module, you will need internet access, if you do not know how to do this from your virtual machine, follow these steps:
    - Create an ssh tunnel, e.g.:  `ssh -D 8888 -f -C -q -N -p 22 {your user name}@lxplus` (this tunnel listen on the local port 8888)
    - Install proxychains: 
        - `curl --socks5-hostname 127.0.0.1:8888 https://codeload.github.com/rofl0r/proxychains-ng/tar.gz/v4.12 > v4.12.tar.gz`
        - `tar xf v4.12.tar.gz`
        - `cd proxychains-ng-4.12`
        - `./configure`
        - `make`
        - `sudo make install`
        - `sudo cp src/proxychains.conf /etc/proxychains.conf`
        - `sudo vi /etc/proxychains.conf` --> adapt the port in the config file to your tunnel port (8888 in this example)
    - upgrade pip (optional step)
        - `sudo /usr/local/bin/proxychains4 pip install --upgrade pip` 
    - Install the gRPC python tools:
        - `sudo /usr/local/bin/proxychains4 pip install grpcio-tools`
        
- Run the script `generateProto.sh` provided in this folder, this script will download the latest version of the proto definition from gitlab and generate the python code

- At this step you should be able to run the python demo client. Make sure that one of the servers exposing flux is started 
([gRPC with reactive streams](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/reactor/ManualFluxServerStarter.java) 
or [Spring Boot](https://gitlab.cern.ch/mgabriel/gRpcDemo/blob/master/server/src/main/java/cern/grpc/demo/SpringBootServerStarter.java))

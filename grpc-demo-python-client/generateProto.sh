#!/usr/bin/env bash
rm -rf proto/*
wget -P proto  https://gitlab.cern.ch/mgabriel/gRpcDemo/raw/master/domain/src/main/proto/DemoService.proto

python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. ./proto/DemoService.proto
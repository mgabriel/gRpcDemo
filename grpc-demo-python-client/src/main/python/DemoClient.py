import grpc
import time

import DemoService_pb2 as DemoService_pb2
import DemoService_pb2_grpc as DemoService_pb2_grpc

class gRPCClient():
    def __init__(self):
        self.channel = grpc.insecure_channel('127.0.0.1:9000')
        self.stub = DemoService_pb2_grpc.DemoServiceStub(self.channel)

    def getSingleResult(self, requestParam):
        return self.stub.getSingleResult(DemoService_pb2.DemoRequest(request=requestParam))

    def getDemoStream(self, requestParam):
        return self.stub.getDemoStream(DemoService_pb2.DemoStreamRequest(request=requestParam))


def main():
    client = gRPCClient()

    singleResult = client.getSingleResult('Hello')
    print(singleResult)
    time.sleep(1)

    streamResult = client.getDemoStream('Hello')
    for re in streamResult:
        print(re)



if __name__ == '__main__':
    main()